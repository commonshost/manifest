const { translate } = require('../capabilities/translate')
const chalk = require('chalk')
const { relative, join, parse } = require('path')
const { readFile, readdirSync, writeFile } = require('fs')
const { promisify } = require('util')
const getStdin = require('get-stdin')

module.exports.command = ['translate']
module.exports.aliases = ['transpile', 'convert', 'export']
module.exports.desc = 'Change a manifest into instructions for a web server or CDN'
module.exports.builder = {
  manifest: {
    alias: 'm',
    type: 'string',
    describe: 'File path of a manifest',
    default: 'serverpush.json'
  },
  format: {
    alias: 'f',
    type: 'string',
    choices: readdirSync(join(__dirname, '../translators'))
      .map((filename) => parse(filename).name),
    describe: 'Supported code generation targets'
  },
  output: {
    alias: 'o',
    type: 'string',
    describe: 'Save output as a file'
  }
}

module.exports.handler = async function handler ({ manifest, format, output }) {
  try {
    const input = manifest
      ? await promisify(readFile)(manifest)
      : await getStdin()
    const generated = await translate(JSON.parse(input), format)
    if (output) {
      await promisify(writeFile)(output, generated)
    } else {
      console.log(generated)
    }
  } catch (error) {
    console.error(chalk.red(`Error: ${error.message}`))
    process.exit(1)
  }
  const filepath = relative(process.cwd(), manifest)
  console.warn(chalk.green(`Generated: ${filepath}`))
}

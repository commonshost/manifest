const { generate } = require('../capabilities/generate')
const { writeFile } = require('fs')
const { relative, join } = require('path')
const { promisify } = require('util')
const { staticDirectories } = require('@commonshost/static-directories')
const directoryExists = require('directory-exists')

module.exports.command = ['generate']
module.exports.aliases = []
module.exports.desc = 'Create a server push manifest for a directory'
module.exports.builder = {
  root: {
    alias: 'r',
    type: 'string',
    describe: 'Directory to process'
  },
  manifest: {
    alias: 'm',
    type: 'string',
    describe: 'File path to save the manifest',
    default: 'serverpush.json'
  }
}

module.exports.handler = async function handler ({ root, manifest }) {
  const cwd = process.cwd()
  if (root === undefined) {
    for (const directory of staticDirectories) {
      const rootpath = join(cwd, directory)
      if (await directoryExists(rootpath)) {
        console.warn(`Detected a static site: ${relative(cwd, rootpath)}`)
        root = rootpath
        break
      }
    }
  }
  if (root === undefined) {
    console.warn('Scanning the current directory')
    root = cwd
  }
  let content
  try {
    content = await generate(root)
  } catch (error) {
    console.error(error.message)
    process.exit(1)
  }
  const json = JSON.stringify(content, null, 2)
  if (manifest) {
    await promisify(writeFile)(manifest, json)
  } else {
    console.log(json)
  }
  const filepath = relative(process.cwd(), manifest)
  console.warn(`Generated the manifest file: ${filepath}`)
}

const { flatten } = require('../helpers/flatten')

async function translate (manifest) {
  let output = 'paths:\n'
  for (const [location, dependencies] of flatten(manifest)) {
    output +=
      `  "${location}":\n` +
      '    mruby.handler: |\n' +
      '      Proc.new do |env|\n' +
      '        [399, {"link" => <<~eos\n'
    for (const dependency of dependencies) {
      output +=
        `          <${dependency}>; rel=preload\n`
    }
    output +=
      '        eos\n' +
      '        }, []]\n' +
      '      end\n'
  }
  return output
}

module.exports.translate = translate

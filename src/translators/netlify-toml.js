const { flatten } = require('../helpers/flatten')
const { cleanUrl } = require('../helpers/cleanUrl')
const { requestDestination } = require('request-destination')

async function translate (manifest) {
  let output = ''
  for (const [location, dependencies] of flatten(manifest)) {
    output +=
      '[[headers]]\n' +
      `  for = "${cleanUrl(location)}"\n` +
      '  [headers.values]\n' +
      `    Link = '''\n`
    const headers = []
    for (const dependency of dependencies) {
      const destination = requestDestination(dependency)
      headers.push(
        `    <${cleanUrl(dependency)}>; ` +
        'rel=preload; ' +
        `as=${destination}`
      )
    }
    output += headers.join(', \\\n') + `'''\n`
  }
  return output
}

module.exports.translate = translate

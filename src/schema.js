module.exports.schema = {
  $id: '/Manifest',
  definitions: {
    url: {
      type: 'string',
      anyOf: [
        { pattern: '^/' },
        { pattern: '^(?:https?:)?//' }
      ]
    }
  },
  type: 'array',
  items: {
    type: 'object',
    additionalProperties: false,
    required: ['get', 'push'],
    properties: {
      get: { '$ref': '#/definitions/url' },
      push: {
        anyOf: [
          { '$ref': '#/definitions/url' },
          {
            type: 'array',
            items: {
              anyOf: [
                { '$ref': '#/definitions/url' },
                {
                  type: 'object',
                  additionalProperties: false,
                  required: ['url'],
                  properties: {
                    url: { '$ref': '#/definitions/url' },
                    priority: { type: 'integer', minimum: 1, maximum: 256 }
                  }
                }
              ]
            }
          }
        ]
      }
    }
  }
}

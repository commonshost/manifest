const { relative } = require('path')
const sherlock = require('@commonshost/sherlock')

// https://tools.ietf.org/html/rfc7540#section-5.3.5
// All streams are initially assigned a non-exclusive dependency on
// stream 0x0.  Pushed streams (Section 8.2) initially depend on their
// associated stream.  In both cases, streams are assigned a default
// weight of 16.

const PRIORITY_HIGH = 256
const PRIORITY_MEDIUM = 128
const PRIORITY_LOW = 1
const PRIORITY_DEFAULT = 16

function getPriority (type) {
  switch (type) {
    case 'application/javascript':
    case 'text/css':
      return PRIORITY_HIGH
    case 'font/*':
      return PRIORITY_MEDIUM
    case 'image/*':
      return PRIORITY_LOW
    default:
      return PRIORITY_DEFAULT
  }
}

const lazyLoadingImage = /(^|\W)(lazy|auto)(\W|$)/i
const absoluteUrl = /https?:\/\//i
const queryStringHashFragment = /^([^?#]+).*/

module.exports.generate =
async function generate (root, { index = 'index.html' } = {}) {
  const parents = new Map()
  for (const dependency of await sherlock(root)) {
    if (lazyLoadingImage.test(dependency.loading)) {
      continue
    }
    const entry = dependency.parent.endsWith(index)
      ? dependency.parent.substr(0, dependency.parent.length - index.length)
      : dependency.parent
    if (!parents.has(entry)) {
      parents.set(entry, new Map())
    }
    const parent = parents.get(entry)
    const priority = getPriority(dependency.type)
    let pathname
    if (absoluteUrl.test(dependency.path)) {
      pathname = dependency.path
    } else {
      const trimQueryHash = dependency.path.replace(queryStringHashFragment, '$1')
      const withinRoot = relative(root, trimQueryHash)
      pathname = `/${withinRoot}`
    }
    if (!parent.has(pathname)) {
      parent.set(pathname, priority)
    }
  }

  const manifest = []
  for (const [get, dependencies] of parents) {
    const entry = `/${relative(root, get)}`
    const rule = { get: entry, push: [] }
    for (const [pathname, priority] of dependencies) {
      const url = pathname
      const push = { url, priority: priority }
      rule.push.push(push) // 🌬
    }
    rule.push.sort(({ priority: a }, { priority: b }) => b - a)
    manifest.push(rule)
  }
  return manifest
}

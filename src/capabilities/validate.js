const Ajv = require('ajv')
const { schema } = require('../schema')

const ajv = new Ajv({ format: 'full' })
ajv.addSchema(schema)

module.exports.validate =
function validate (json) {
  const validity = ajv.validate('/Manifest', json)
  if (validity === true) {
    return true
  } else {
    const error = new Error(ajv.errors.map(
      (error) => `${error.dataPath} ${error.message}`
    ).join('\n'))
    error.ajv = ajv.errors
    throw error
  }
}

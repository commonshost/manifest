const cached = new WeakMap()

function normalise (manifest) {
  if (cached.has(manifest)) return cached.get(manifest)
  const normalised = (manifest || []).map((rule) => {
    if (typeof rule.get !== 'string') return
    if (typeof rule.push === 'string') rule.push = [{ url: rule.push }]
    if (!Array.isArray(rule.push)) return
    return {
      get: rule.get,
      push: rule.push.reduce((push, dep) => {
        if (typeof dep === 'string') {
          push.push({ url: dep })
        } else if (typeof dep.url === 'string') {
          if (typeof dep.priority === 'number') {
            push.push({ url: dep.url, priority: dep.priority })
          } else {
            push.push({ url: dep.url })
          }
        }
        return push
      }, [])
    }
  }).filter((rule) => rule && rule.get && rule.push && rule.push.length)

  if (manifest) cached.set(manifest, normalised)

  return normalised
}

module.exports.normalise = normalise

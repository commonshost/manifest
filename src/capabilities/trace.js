const { normalise } = require('./normalise')

function trace (manifest, entry, dependencies = new Map()) {
  _trace(normalise(manifest), entry, dependencies)
  return dependencies
}

function _trace (manifest, entry, dependencies) {
  for (const { get, push } of manifest) {
    if (get === entry) {
      for (const { url, priority } of push) {
        if (!dependencies.has(url)) {
          dependencies.set(url, priority)
          _trace(manifest, url, dependencies)
        }
      }
    }
  }
}

module.exports.trace = trace

function flatten (manifest) {
  const results = new Map()
  for (const rule of manifest) {
    const dependencies = new Set()
    for (const push of rule.push) {
      dependencies.add(push.url)
    }
    results.set(rule.get, dependencies)
  }
  return results
}

module.exports.flatten = flatten

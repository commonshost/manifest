#!/usr/bin/env node

require('hard-rejection/register')
const yargs = require('yargs')
const updateNotifier = require('update-notifier')
const normalizeData = require('normalize-package-data')
const { fromUrl } = require('hosted-git-info')
const pkg = require('../package.json')
const { join } = require('path')

async function cli () {
  normalizeData(pkg)
  process.title = Object.keys(pkg.bin)[0]
  updateNotifier({ pkg }).notify()
  await yargs
    .usage('Usage: $0 <command>')
    .example('$0 --help <command>', 'Show command-specific options')
    .epilogue(`Documentation:\n  ${fromUrl(pkg.repository.url).docs()}`)
    .commandDir(join(__dirname, 'commands'))
    .demandCommand(1)
    .help('help').alias('h', 'help')
    .version().alias('v', 'version')
    .wrap(null)
    .parse(process.argv.slice(2))
}

cli()

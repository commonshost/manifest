const test = require('blue-tape')
const { promisify } = require('util')
const fs = require('fs')
const { exec } = require('child_process')
const { join, resolve } = require('path')
const { dir } = require('tmp')

const node = process.execPath
const cli = resolve(__dirname, '../src/cli.js')

test('check the presence of manifest after running command', async (t) => {
  const temp = await promisify(dir)({ unsafeCleanup: true })
  const root = join(__dirname, 'fixtures/generate')
  const manifest = join(temp, 'test.json')
  const command = `${node} -- ${cli} generate -r ${root} -m ${manifest}`
  await promisify(exec)(command)
  const actual = await promisify(fs.readFile)(manifest, 'utf8')
  const fixture = join(__dirname, 'fixtures/cli-generate.json')
  const expected = await promisify(fs.readFile)(fixture, 'utf8')
  t.deepEqual(actual, expected)
})

test('default output manifest file', async (t) => {
  const temp = await promisify(dir)({ unsafeCleanup: true })
  const root = join(__dirname, 'fixtures/generate')
  const manifest = join(temp, 'serverpush.json')
  const command = `${node} -- ${cli} generate --root ${root}`
  const cwd = temp
  await promisify(exec)(command, { cwd })
  const actual = await promisify(fs.readFile)(manifest, 'utf8')
  const fixture = join(__dirname, 'fixtures/cli-generate.json')
  const expected = await promisify(fs.readFile)(fixture, 'utf8')
  t.deepEqual(actual, expected)
})

test('default input root, detect static site', async (t) => {
  const temp = await promisify(dir)({ unsafeCleanup: true })
  const fixtures = join(__dirname, 'fixtures/static-site-generator')
  const manifest = join(temp, 'actual.json')
  const command = `${node} -- ${cli} generate --manifest ${manifest}`
  const cwd = fixtures
  await promisify(exec)(command, { cwd })
  const actual = await promisify(fs.readFile)(manifest, 'utf8')
  const fixture = join(fixtures, 'expected.json')
  const expected = await promisify(fs.readFile)(fixture, 'utf8')
  t.deepEqual(actual, expected)
})

test('write to standard output if path is empty string', async (t) => {
  const root = join(__dirname, 'fixtures/generate')
  const command = `${node} -- ${cli} generate --root ${root} --manifest ''`
  const cwd = root
  const { stdout: actual } = await promisify(exec)(command, { cwd })
  const fixture = join(__dirname, 'fixtures/cli-generate.json')
  const expected = await promisify(fs.readFile)(fixture, 'utf8')
  t.deepEqual(actual, expected + '\n')
})

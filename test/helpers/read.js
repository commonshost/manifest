const { join } = require('path')
const { readFile } = require('fs')
const { promisify } = require('util')

async function read (...paths) {
  return promisify(readFile)(join(...paths), 'utf8')
}

module.exports.read = read

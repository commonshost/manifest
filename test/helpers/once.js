function once (target, type) {
  return new Promise((resolve) => {
    target.once(type, resolve)
  })
}

module.exports.once = once

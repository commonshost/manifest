const test = require('blue-tape')
const { trace } = require('..')
const { deepStrictEqual } = require('assert')

const PRIORITY_DEFAULT = undefined
const PRIORITY_HIGH = 256
const PRIORITY_LOW = 1

const fixtures = [
  {
    scenario: 'Nested dependencies',
    given: {
      entry: '/entry',
      manifest: [
        { get: '/entry', push: '/foo.js' },
        { get: '/foo.js', push: '/bar.js' }
      ]
    },
    expected: new Map([
      ['/foo.js', PRIORITY_DEFAULT],
      ['/bar.js', PRIORITY_DEFAULT]
    ])
  },
  {
    scenario: 'Custom priority',
    given: {
      entry: '/entry',
      manifest: [
        {
          get: '/entry',
          push: [
            { url: '/foo.js', priority: PRIORITY_HIGH },
            { url: '/bar.js', priority: PRIORITY_HIGH },
            { url: '/foo.css' },
            { url: '/foo.svg', priority: PRIORITY_LOW }
          ]
        }
      ]
    },
    expected: new Map([
      ['/foo.js', PRIORITY_HIGH],
      ['/bar.js', PRIORITY_HIGH],
      ['/foo.css'],
      ['/foo.svg', PRIORITY_LOW]
    ])
  }
]

for (
  const {
    scenario,
    given: { manifest, entry },
    expected
  } of fixtures
) {
  test(scenario, async (t) => {
    const actual = trace(manifest, entry)
    t.doesNotThrow(() => deepStrictEqual(actual, expected))
  })
}

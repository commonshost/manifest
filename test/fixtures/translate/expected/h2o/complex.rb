paths:
  "/index.html":
    mruby.handler: |
      Proc.new do |env|
        [399, {"link" => <<~eos
          </app.js>; rel=preload
          </images/photo.jpg>; rel=preload
        eos
        }, []]
      end
  "/about.html":
    mruby.handler: |
      Proc.new do |env|
        [399, {"link" => <<~eos
          </images/photo.jpg>; rel=preload
        eos
        }, []]
      end
  "/app.js":
    mruby.handler: |
      Proc.new do |env|
        [399, {"link" => <<~eos
          </lib.js>; rel=preload
        eos
        }, []]
      end

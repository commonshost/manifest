const test = require('blue-tape')
const { spawn } = require('child_process')
const { createReadStream } = require('fs')
const { join, extname, parse, resolve } = require('path')
const { once } = require('./helpers/once')
const { concat } = require('./helpers/concat')
const { read } = require('./helpers/read')
const { list } = require('./helpers/list')
const { dir } = require('tmp')
const { promisify } = require('util')

const node = process.execPath
const cli = resolve(__dirname, '../src/cli.js')
const args = ['--', cli, 'translate']
const cwd = join(__dirname, 'fixtures')

async function run (options, cwd) {
  const child = spawn(node, [...args, ...options], { cwd })
  const [stdout, stderr] = await Promise.all([
    concat(child.stdout), concat(child.stderr)
  ])
  await once(child, 'close')
  return { stdout, stderr, child }
}

test('Read from file and save to file', async (t) => {
  const fixtures = join(__dirname, 'fixtures/translate')
  const scenarios = await list(fixtures, 'given')
  const formats = await list(fixtures, 'expected')
  t.ok(formats.length > 0)
  t.ok(scenarios.length > 0)

  for (const scenario of scenarios) {
    const filename = parse(scenario).name
    for (const format of formats) {
      const extension = extname((await list(fixtures, 'expected', format))[0])
      test(`Generate ${format} configuration for "${filename}"`, async (t) => {
        const { stdout, stderr } = await run([
          '--manifest', join(fixtures, 'given', scenario),
          '--format', format
          // '--output', ''
        ], cwd)
        const file = filename + extension
        const expected = await read(fixtures, 'expected', format, file)
        t.equal(stdout, expected + '\n')
        t.ok(/Generated/.test(stderr))
      })
    }
  }
})

test('Missing format', async (t) => {
  const { stderr } = await run([
    '--manifest', join(__dirname, 'fixtures/translate/given/docs.json')
    // '--format', ''
    // '--output', ''
  ], cwd)
  t.ok(/Format not supported/.test(stderr))
})

test('Read from stdin', async (t) => {
  const child = spawn(node, [
    ...args,
    '--manifest', '',
    '--format', 'apache'
    // '--output', ''
  ])
  const given = join(cwd, 'translate/given/docs.json')
  createReadStream(given).pipe(child.stdin)
  const stdout = await concat(child.stdout)
  await once(child, 'close')
  const expected = await read(cwd, 'translate/expected/apache/docs.conf')
  t.equal(stdout, expected + '\n')
})

test('Save to output file', async (t) => {
  const temp = await promisify(dir)({ unsafeCleanup: true })
  const output = join(temp, 'httpd.conf')
  const { stdout, stderr } = await run([
    '--manifest', join(__dirname, 'fixtures/translate/given/docs.json'),
    '--format', 'apache',
    '--output', join(temp, 'httpd.conf')
  ], cwd)
  const actual = await read(output)
  const expected = await read(cwd, 'translate/expected/apache/docs.conf')
  t.equal(actual, expected)
  t.equal(stdout, '')
  t.ok(/Generated/.test(stderr))
})

const test = require('blue-tape')
const { normalise } = require('..')
const { performance, PerformanceObserver } = require('perf_hooks')

test('Memoise for significantly better repeat performance', async (t) => {
  const manifest = [
    { get: '/foo', push: ['/bar', { url: '/bla', priority: 256 }] }
  ]
  const wrapped = performance.timerify(normalise)
  let entries = []
  const obs = new PerformanceObserver((list) => {
    entries.push(...list.getEntries())
  })
  obs.observe({ entryTypes: ['function'] })
  wrapped(manifest)
  wrapped(manifest)
  wrapped(manifest)
  wrapped(manifest)
  wrapped(manifest)
  obs.disconnect()
  t.ok(entries.length > 1)
  const first = entries[0]
  for (const repeat of entries.slice(1)) {
    t.ok(first.duration > 5 * repeat.duration)
  }
})

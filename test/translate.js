const test = require('blue-tape')
const { join, extname, parse } = require('path')
const { read } = require('./helpers/read')
const { list } = require('./helpers/list')
const { translate } = require('..')

test('Translate API', async (t) => {
  const fixtures = join(__dirname, 'fixtures/translate')
  const scenarios = await list(fixtures, 'given')
  const formats = await list(fixtures, 'expected')
  t.ok(formats.length > 0)
  t.ok(scenarios.length > 0)

  for (const scenario of scenarios) {
    const filename = parse(scenario).name
    for (const format of formats) {
      const extension = extname((await list(fixtures, 'expected', format))[0])
      test(`Generate ${format} configuration for "${filename}"`, async (t) => {
        const given = await read(fixtures, 'given', scenario)
        const manifest = JSON.parse(given)
        const actual = await translate(manifest, format)
        const file = filename + extension
        const expected = await read(fixtures, 'expected', format, file)
        t.equal(actual, expected)
      })
    }
  }
})

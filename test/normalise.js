const test = require('blue-tape')
const { normalise, validate } = require('..')

// Normalised:
// get: '...',
// push: [
//   {
//     url: '...',
//     priority: 16
//   }
// ]
// })

// get: 'a'                                 -> 'a'
// push: 'a'                                -> [{ url: 'a' }]
// push: ['a']                              -> [{ url: 'a' }]
// push: ['a', 'b']                         -> [{ url: 'a' }, { url: 'b' }]
// push: [{ url: 'a', priority: 256 }]      -> [{ url: 'a', priority: 256 }]
// push: [{ url: 'a', priority: 256 }, 'b'] -> [{ url: 'a', priority: 256 }, { url: 'b' }]

const fixtures = [
  {
    scenario: 'Empty manifest',
    given: [],
    expected: []
  },
  {
    scenario: 'Undefined manifest',
    given: undefined,
    expected: []
  },
  {
    scenario: 'Drop pointless rules',
    given: [
      {},
      { get: 'foo' },
      { push: '/foo' }
    ],
    expected: []
  },
  {
    scenario: 'Already normalised',
    given: [{
      get: '/foo',
      push: [{ url: '/bar' }]
    }],
    expected: [{
      get: '/foo',
      push: [{ url: '/bar' }]
    }]
  },
  {
    scenario: 'Custom priority',
    given: [{
      get: '/foo',
      push: [{ url: '/foo', priority: 256 }]
    }],
    expected: [{
      get: '/foo',
      push: [{ url: '/foo', priority: 256 }]
    }]
  },
  {
    scenario: 'Push as a string',
    given: [{
      get: '/foo',
      push: '/bar'
    }],
    expected: [{
      get: '/foo',
      push: [{ url: '/bar' }]
    }]
  },
  {
    scenario: 'Push as array of strings',
    given: [{
      get: '/foo',
      push: ['/bar', '/lol']
    }],
    expected: [{
      get: '/foo',
      push: [{ url: '/bar' }, { url: '/lol' }]
    }]
  },
  {
    scenario: 'Strip other properties',
    given: [{
      get: '/foo',
      push: [{ url: '/bar', extra: 'struff' }]
    }],
    expected: [{
      get: '/foo',
      push: [{ url: '/bar' }]
    }]
  },
  {
    scenario: 'Ignore non-numeric priority',
    given: [{
      get: '/foo',
      push: [{ url: '/bar', priority: 'bla' }]
    }],
    expected: [{
      get: '/foo',
      push: [{ url: '/bar' }]
    }]
  }
]

for (const { scenario, given, expected } of fixtures) {
  test(`Normalise API: ${scenario}`, async (t) => {
    const actual = normalise(given)
    t.deepEqual(actual, expected)
    t.doesNotThrow(() => validate(actual))
  })
}

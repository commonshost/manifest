const test = require('blue-tape')
const { validate } = require('..')

const fixtures = {
  'Single and multiple push url': [
    { get: '/', push: '/bar' },
    { get: '/', push: ['/bar', '/bla'] }
  ],

  'Mixed push string and object': [
    { get: '/', push: ['/bar', { url: '/bla' }] }
  ]
}

for (const label of Object.keys(fixtures)) {
  let index = 0
  for (const given of fixtures[label]) {
    index++
    test(`Valid manifest: ${label} (${index})`, async (t) => {
      t.doesNotThrow(() => {
        validate([given])
      })
    })
  }
}

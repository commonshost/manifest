const test = require('blue-tape')
const { join } = require('path')
const { generate, validate } = require('..')

test('Generate a manifest', async (t) => {
  const expected = [
    {
      get: '/loading.html',
      push: [
        { url: '/loading-default.png', priority: 1 },
        { url: '/loading-eager.png', priority: 1 },
        { url: 'https://example.net', priority: 1 }
      ]
    },
    {
      get: '/dummy.css',
      push: [
        { url: '/high_priority.css', priority: 256 },
        { url: '/high_priority.woff2', priority: 128 },
        { url: '/medium_priority.svg', priority: 1 }
      ]
    }
  ]
  const actual = await generate(join(__dirname, 'fixtures/generate'))
  t.deepEqual(actual, expected)
  t.doesNotThrow(() => validate(actual))
})

# @commonshost/manifest

**HTTP Server Push Manifest** is a configuration format for web servers, CDNs, and web development tools.

Use the API or CLI tool to generate the manifest from static HTML, JS, and CSS files.

Manifest are supported natively by [Commons Host](https://commons.host). Manifests can be transpiled to many other servers and CDNs.

```js
[
  {
    get: '/app.html',
    push: '/styles/design.css'
  },
  {
    get: '/styles/design.css',
    push: [
      '/images/logo.svg',
      '/images/banner.jpg',
      {
        url: '/fonts/Oswald-ExtraLight.otf',
        priority: 256
      },
      {
        url: '/ads/banner.png',
        priority: 1
      }
    ]
  }
]
```

## Format

A **manifest** is a JSON array containing rules. Each **rule** is a JSON object containing a `get` and `push` property. These are the **trigger** and **action** respectively.

### Trigger `get`

The pathname (URL) of a request. If this matches, the trigger fires its push action.

### Action `push`

One or more resources to push from server to client. A push action has `url` and `priority` properties. The `url` value is a pathname of a local resource. The `priority` value is an integer from `1` through `256` and defaults to `16`.

## Supported Platforms

A manifest is an intermediary format which can be transpiled to the configuration syntax of CDN services or web servers.

**CDN services**: Commons Host (native), Cloudflare, Fastly, Netlify

**Web servers**: Apache, H2O, NGINX

**Platforms**: Node.js

**Browsers**: All modern web browsers support HTTP Server Push. The manifest is used from the server side.
